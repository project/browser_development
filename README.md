### INTRODUCTION
Browser development allows a developer to create SCSS files and store the data
in entity configuration for version control purposes. As the configuration is portable
and created in a component style it can be used on multiple projects.

If used in conjunction with
[layout_builder_styles](https://www.drupal.org/project/layout_builder_styles)
it allows an editor to add any class to a block or section using the layout
manager giving them the ability to create pages that are highly flexible and
needing less assistance from a developer.

The module can be used with any theme and the compiled scss creates a css file
that gets injected into the html page. However, the css file is static and is
not removed from your file system. This allows the developer to uninstall the
module on production, and add the css file to a custom theme increasing site
performance.

#### REQUIREMENTS

Drupal 10 or 11 are needed as well as 'scssphp/scssphp' library which is installed
in the vendor directory when using composer.

#### INSTALLATION

Install using composer then using Drush or the Drupal UI enable the module.

#### CONFIGURATION

There is no configuration just install and navigate to
`admin/browser-development/editor`
