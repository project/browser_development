(function ($) {

  var initialized;

  //-- Stops Drupal from loading JS multiple times
  function init() {
    if (!initialized) {
      initialized = true;
    }
  }



  Drupal.behaviors.browser_development = {
    attach: function (context, settings) {
      'use strict';
      init();
    }
  };
})(jQuery);
