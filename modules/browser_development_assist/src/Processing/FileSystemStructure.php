<?php

namespace Drupal\browser_development_assist\Processing;

/**
 * Class FileSystemStructure controls the file system array.
 *
 * @package Drupal\browser_development_assist\Processing
 */
class FileSystemStructure {

  /**
   * Global array that holds all relevant paths.
   *
   * @var array
   *   Sets global path.
   */
  public static array $globalFilePathArray = [
    'base_path' => 'public://browser-development/',
    'css_directory' => 'css',
    'uri_path' => 'files/browser-development/css',
    'css_name' => [
      'default' => 'default.css',
      'inline' => 'inline.css',
      'admin' => 'admin.css'
    ],
    'scss_directory' => 'scss',
    'scss_directory_children' => ['default','inline','admin'],
  ];


  /**
   * Returns path of css file to be used.
   *
   * @return string
   *   Returns path of css file.
   */
  public static function getCssFilePath($file_name = 'default'): string {
    $site_path = \Drupal::service('kernel')->getSitePath();
    return DIRECTORY_SEPARATOR
      . $site_path
      . DIRECTORY_SEPARATOR
      . self::$globalFilePathArray['uri_path']
      . DIRECTORY_SEPARATOR
      . self::$globalFilePathArray['css_name'][$file_name];
  }

}
