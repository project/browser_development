<?php

namespace Drupal\browser_development\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class gives developer an entry point with relevant information.
 *
 * @package Drupal\browser_development\home
 */
class Home extends ControllerBase {

  /**
   * Returns browser development homepage information.
   *
   * @return array
   *   Array of markup.
   */
  public function getContent() {

    $output = "<p>Browser development allows a developer to create SCSS files ";
    $output .= "and store the data in configuration for version control ";
    $output .= "purposes.</p>";
    $output .= "<h5>Links</h5>";
    $output .= "<p><a href=\"browser-development/editor\">Editor</a><br/>";
    $output .= "<a href=\"browser-development/settings\">Settings</a><br/>";
    $output .= "<a href=\"browser-development/storage\">Storage</a></p>";
    $output .= "<h5>Issues</h5>";
    $output .= "<p><a href=\"https://www.drupal.org/project/issues/browser_development?categories=All\">Drupal Issue Queue</a></p>";

    return [
      '#type' => 'markup',
      '#markup' => $this->t($output),
    ];
  }

}
