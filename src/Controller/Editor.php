<?php

namespace Drupal\browser_development\Controller;

use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Template\TwigEnvironment;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\browser_development\Processing\FormsStorage;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class Editor controller allows access to ReactJs editor.
 *
 * @package Drupal\browser_development\Editor
 */
class Editor extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Twig object.
   *
   * @var TwigEnvironment
   */
  protected $twig;

  /**
   * Data sent to controller.
   *
   * @var array
   */
  protected array $dataArray;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Template\TwigEnvironment $twig
   *   Twig object.
   */
  public function __construct(TwigEnvironment $twig) {
    $this->twig = $twig;
  }

  /**
   * Twig Container.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Twig Container.
   *
   * @return static
   *   Twig Container object.
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('twig'));
  }

  /**
   * Gets current domain.
   *
   * @todo Remove inline CurrentLanguage and use dependency injection.
   *
   * @return string
   *   Domain that is assigned to the front page.
   */
  protected function domainPath(): string {
    $url_options = [
      'absolute' => TRUE,
      'language' => \Drupal::languageManager()->getCurrentLanguage(),
    ];
    return Url::fromRoute('<front>', [], $url_options)->toString();
  }

  /**
   * Sets up assets paths for page rendering.
   *
   * @return array
   *   Array of paths for template rendering variables.
   */
  protected function templateVariables(): array {

    $url = "";
    if (!empty(FormsStorage::getStorage('browser_development_settings')['browser_development_settings']['uri'])) {
      $url = FormsStorage::getStorage('browser_development_settings')['browser_development_settings']['uri'];
    }

    $js = "";
    if (!empty(FormsStorage::getStorage('browser_development_settings')['browser_development_settings']['js'])) {
      $js = FormsStorage::getStorage('browser_development_settings')['browser_development_settings']['js'];
    }

    $this->dataArray = [
      'http_url' => $this->domainPath(),
      'module_path' => \Drupal::service('extension.list.module')->getPath('browser_development'),
      'api_url' => $url,
      'js_library' => $js,
    ];

    $this->dataArray['assets_url'] = $this->dataArray['http_url']
                          . $this->dataArray['module_path'];

    return $this->dataArray;
  }

  /**
   * Function for debugging.
   *
   * @return string
   *   Returns data structure for troubleshooting
   */
  protected function templateVariablesDebug(): string {
    return var_export($this->dataArray, TRUE);
  }

  /**
   * Returns template with Editor attached.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Template that needs to be returned.
   * @throws \Throwable
   */
  public function getContent(): Response {

    $drupalModulePath = \Drupal::service('extension.list.module')->getPath('browser_development');
    $response = new Response();
    $twigFilePath = $drupalModulePath . '/templates/bd-editor.html.twig';
    $templateClass = $this->twig->getTemplateClass( $twigFilePath);
    $template = $this->twig->loadTemplate($templateClass,  $twigFilePath);
    $markup = $template->render([
      'data_array' => [
        'data' => $this->templateVariables(),
        'data_debug' => $this->templateVariablesDebug(),
      ],
    ]);
    $response->headers->set('Content-Type', 'text/html; charset=utf-8');
    $response->setContent($markup);
    return $response;
  }

}
