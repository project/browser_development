<?php

namespace Drupal\browser_development\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


use Drupal\browser_development\Processing\Storage;
use Drupal\browser_development\Processing\ScssCompiler;
use Drupal\browser_development\Processing\LiveScssCompiler;

/**
 * Class API controller gives the editor an integration endpoint.
 *
 * @package Drupal\browser_development\Api
 */
class Api extends ControllerBase {

  /**
   * Returns template with Editor attached.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Template that needs to be returned.
   */
  public function getApi(Request $request) {
    $data = json_decode($request->getContent(), TRUE);

    if (json_last_error() !== 0) {
      return new JsonResponse([
        "api_response" => $this->t("Json request was not completed"),
      ]);
    }

    switch ($data) {
      case isset($data['live']):
        $lscObj = new LiveScssCompiler();
        $return = $lscObj->compiler($data);
        break;

      case isset($data['compiled']):
        Storage::setStorage($data);
        $scObj = new ScssCompiler();
        $return = $scObj->compiler($data);
        break;

      case isset($data['open']):
        $return = Storage::getStorage($data);
        break;

      default:
        $return = [
          "api_response" => $this->t("Request was not completed"),
        ];
    }

    return new JsonResponse($return);
  }

}
