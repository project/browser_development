<?php

namespace Drupal\browser_development;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Browser development storage entities.
 */
class BrowserDevelopmentStorageListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Date created');
    $header['json_ob'] = $this->t('Json Object');
    // $header['id'] = $this->t('Machine name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $splitE = $entity->label();
    $row['label'] = str_replace('__', '   ', $splitE);
    $row['json_obj'] = $entity->jsonObj();
    // You probably want a few more properties here...
    return $row + parent::buildRow($entity);
  }

}
