<?php

namespace Drupal\browser_development\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Browser development storage entities.
 */
interface BrowserDevelopmentStorageInterface extends ConfigEntityInterface {

  /**
   * The Browser development storage jsonObj.
   *
   * @return string
   *   This is a serialize string
   */
  public function jsonObj();

}
