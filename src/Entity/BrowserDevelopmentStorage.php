<?php

namespace Drupal\browser_development\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Browser development storage entity.
 *
 * @ConfigEntityType(
 *   id = "browser_development_storage",
 *   label = @Translation("Browser development storage"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\browser_development\BrowserDevelopmentStorageListBuilder",
 *     "form" = {
 *       "add" = "Drupal\browser_development\Form\BrowserDevelopmentStorageForm",
 *       "edit" = "Drupal\browser_development\Form\BrowserDevelopmentStorageForm",
 *       "delete" = "Drupal\browser_development\Form\BrowserDevelopmentStorageDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\browser_development\BrowserDevelopmentStorageHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "browser_development_storage",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "json_obj = json_obj"
 *   },
 *   links = {
 *     "canonical" = "/admin/browser-development/storage/{browser_development_storage}",
 *     "add-form" = "/admin/browser-development/storage/add",
 *     "edit-form" = "/admin/browser-development/storage/{browser_development_storage}/edit",
 *     "delete-form" = "/admin/browser-development/storage/{browser_development_storage}/delete",
 *     "collection" = "/admin/browser-development/storage"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "json_obj"
 *   }
 * )
 */
class BrowserDevelopmentStorage extends ConfigEntityBase implements BrowserDevelopmentStorageInterface {

  /**
   * The Browser development storage ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Browser development storage label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Browser development storage jsonObj.
   *
   * @var string
   */
  protected $json_obj;

  /**
   * {@inheritdoc}
   */
  public function jsonObj() {

    return $this->json_obj;
  }

}
