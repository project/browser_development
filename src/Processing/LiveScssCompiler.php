<?php

namespace Drupal\browser_development\Processing;

use ScssPhp\ScssPhp\Compiler;
use ScssPhp\ScssPhp\OutputStyle;

/**
 * LiveScssCompiler returns compressed CSS for live updates in the editor.
 *
 * @package Drupal\browser_development\Processing
 */
class LiveScssCompiler {

  /**
   * Compiles SCSS to CSS.
   *
   * Method compiles SCSS to CSS of the the live editor, it's compressed
   * ensuring that data is returned in a performante way.
   *
   * @param mixed $data
   *   Scss data to be compiled.
   *
   * @return JsonResponse
   *   Json response with compiled CSS.
   *
   * @throws \Drupal\browser_development\Processing\SassScriptException
   *   Throws SCSS compile error.
   * @throws Error
   *   Throws general errors during SCSS process.
   */
  public function compiler($data) {

    try {
      $compiler = new Compiler();
      $compiler->setOutputStyle(OutputStyle::COMPRESSED);
      $result = $compiler->compileString($data['live'])->getCss();
      return [
        "live_response" => $result,
      ];

    }
    catch (SassScriptException $e) {
      throw $e;
    }
    catch (Exception $e) {
      throw new Error('An exception was thrown', 0, $e);
    }
  }

}
