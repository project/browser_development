<?php

namespace Drupal\browser_development\Processing;


/**
 * Class FileSystemStructure controls the file system array.
 *
 * @todo Have the ability to change the paths need alter where items are
 * compiled and saved on the file system.
 *
 * @package Drupal\browser_development\Processing
 */
class FileSystemStructure {

  /**
   * Global array that holds all relevant paths.
   *
   * @var array
   *   Sets global path.
   */
  public array $globalFilePathArray = [
    'base_path' => 'public://browser-development/',
    'css_directory' => 'css',
    'uri_path' => 'files/browser-development/css',
    'css_name' => [
      'default' => 'default.css',
      'inline' => 'inline.css',
      'admin' => 'admin.css'
    ],
    'scss_directory' => 'scss',
    'scss_directory_children' => ['default','inline','admin'],
  ];

  /**
   * File system full path.
   *
   * @var string
   *   Finds absolute path for CSS directory.
   */
  public string $absolutePath;

  /**
   * Customers CSS styles.
   *
   * @param array $globalFilePathArray
   *   Allows developer to override the path.
   */
  public function __construct(array $globalFilePathArray = []) {
    if (!empty($globalFilePathArray)) {
      $this->globalFilePathArray = $globalFilePathArray;
    }
    if (!empty(FormsStorage::getStorage('browser_development_settings')['browser_development_settings'])) {
      $this->globalFilePathArray['base_path'] = FormsStorage::getStorage('browser_development_settings')['browser_development_settings']['base_path'];
    }
    $this->getAbsolutePath();
    $this->createFileAndDirectoryStructure();
  }

  /**
   * Gets Absolute Path for CSS file creation.
   */
  private function getAbsolutePath() {

    $this->absolutePath = \Drupal::service('file_system')->realpath(
            $this->globalFilePathArray['base_path']);
  }

  /**
   * Path to stored CSS compiled SCSS file.
   *
   * @todo This is not being used remove in future releases.
   *
   * @return string
   *   Provides string to path of file.
   */
  protected function pathToStoredFile(): string {
    $host = str_replace(".", "-", \Drupal::request()->getHost());
    $public = $this->globalFilePathArray['base_path'];
    $css = $this->globalFilePathArray['css_directory'];
    $file = $this->globalFilePathArray['css_name'];
    return $public . DIRECTORY_SEPARATOR . $css . DIRECTORY_SEPARATOR . $host . '-' . $file;
  }

  /**
   * Creates public directory for CSS file if needed.
   *
   * @param string $data
   *   Data to be added to file.
   */
  protected function writeToFile(string $data = "{}") {
    $fileOpen = fopen($this->getCssFilePath(), "wb");
    fwrite($fileOpen, $data);
    fclose($fileOpen);
  }

  /**
   * Creates public directory for CSS file if needed .
   */
  private function createFileAndDirectoryStructure(): void {

    $public = $this->globalFilePathArray['base_path'];
    $css = $this->globalFilePathArray['css_directory'];
    $scss = $this->globalFilePathArray['scss_directory'];
    $scss_children = $this->globalFilePathArray['scss_directory_children'];

    if (!is_dir($public)) {
      $fileSystem = \Drupal::service('file_system');
      $fileSystem->mkdir($public, 0775);
      $fileSystem->mkdir($public . $css, 0775);
      $fileSystem->mkdir($public . $scss, 0775);
      foreach ($scss_children as $child) {
        $fileSystem->mkdir($public . $scss . DIRECTORY_SEPARATOR. $child, 0775);
      }
      $path = $this->pathToStoredFile();
      $this->writeToFile($path);
    }
  }

  /**
   * Get hat configuration.
   *
   * @param array $path
   *   Set path array if needed.
   */
  public function setPathArray(array $path = []): array {

    if (!empty($path)) {
      $this->globalFilePathArray['base_path'] = $path;
      $arr['base_path'] = $this->globalFilePathArray['base_path'];
      FormsStorage::setStorage('browser_development_settings', $arr);
    }
  }

  /**
   * Returns array path.
   *
   * @return array
   *   Returns array path.
   */
  public function getPathArray() {

    return $this->globalFilePathArray;
  }

  /**
   * Returns path of css file to be used.
   *
   * @return string
   *   Returns path of css file.
   */
  public function getCssFilePath($file_name = 'default') {
    $site_path = \Drupal::service('kernel')->getSitePath();
    return DRUPAL_ROOT
      . DIRECTORY_SEPARATOR
      . $site_path
      . DIRECTORY_SEPARATOR
      . $this->globalFilePathArray['uri_path']
      . DIRECTORY_SEPARATOR
      . $this->globalFilePathArray['css_name'][$file_name];
  }

}
