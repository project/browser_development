<?php

namespace Drupal\browser_development\Processing;

use ScssPhp\ScssPhp\Compiler;
use ScssPhp\ScssPhp\OutputStyle;

/**
 * Class ScssCompiler compiles SCSS to CSS.
 *
 * @package Drupal\browser_development\Processing
 */
class ScssCompiler extends FileSystemStructure {

  /**
   * Data that needs to be parsed to be compiled.
   *
   * @var array
   *   Adds data for SCSS deployment.
   */
  protected array $data;

  /**
   * Array that hold files names so they can be compiled.
   *
   * @var array
   *   Compile order data for SCSS deployment.
   */
  protected array $compileOrderArray = [];

  /**
   * End result after compiler is completed.
   *
   * @var string
   *    CSS string created by SCSS Compiler.
   */
  protected string $compiledScss;

  /**
   * Get the full path to the SCSS directory.
   *
   * @return string
   *   The full path to the SCSS directory.
   */
  protected function getScssDirectoryPath(): string {
    $directory = $this->globalFilePathArray['scss_directory'];
    return $this->absolutePath . DIRECTORY_SEPARATOR . $directory;
  }


  /**
   * Clears SCSS directory before compiling SCSS.
   */
  protected function clearDirectory(): void {
    $files = glob($this->getScssDirectoryPath() . DIRECTORY_SEPARATOR . "*");
    foreach ($files as $file) {
      if (is_file($file)) {
        unlink($file);
      }
    }
  }

  /**
   * Creates files with data before it gets compiled.
   *
   * @param array $file
   *   Array that holds filename and data for the file.
   */
  protected function createFilesAndAddData(array $file): void {
    $filePlaceHolder = empty($file['title']) ? rand(1, 999) : $file['title'];
    $fileName = str_replace(' ', '-', strtolower($filePlaceHolder)) . ".scss";
    $this->compileOrderArray[] = $fileName;
    $path = $this->getScssDirectoryPath() . DIRECTORY_SEPARATOR . $fileName;
    if ($fileOpen = fopen($path, "wb")) {
      fwrite($fileOpen, $file['code']);
      fclose($fileOpen);
      chmod($path, 0755);
    } else {
      throw new \RuntimeException("Unable to create and write to file: {$path}");
    }
  }

  /**
   * Creates main.scss import file and then compiles files.
   *
   * @throws \ScssPhp\ScssPhp\Exception\SassException
   */
  protected function compileFiles() {
    $fileName = "main.scss";
    $path = $this->getScssDirectoryPath() . DIRECTORY_SEPARATOR . $fileName;

    if ($fileOpen = fopen($path, "wb")) {
      foreach ($this->compileOrderArray as $fileOrder) {
        fwrite($fileOpen, "@import \"$fileOrder\"; ");
      }
      fclose($fileOpen);
    } else {
      throw new \RuntimeException("Unable to create and write to file: {$path}");
    }

    $compiler = new Compiler();
    $compiler->setOutputStyle(OutputStyle::COMPRESSED);
    $compiler->setImportPaths($this->getScssDirectoryPath());
    $this->compiledScss = $compiler->compileString('@import "main.scss";')->getCss();
  }

  /**
   * Compiles SCSS to CSS.
   *
   * @param array $data
   *   Data to be compiled.
   *
   * @return array
   *   Returns compiled response to user.
   * @throws \ScssPhp\ScssPhp\Exception\SassException
   */
  public function compiler(array $data): array {
    $grouped_array = [];
    foreach ($data['compiled'] as $item) {
      $css_file_value = $item['configuration']['css_files']['value'];
      $grouped_array[$css_file_value][] = $item;
    }

    $this->clearDirectory();

    foreach ($grouped_array as $css_file_value_key => $files) {
      foreach ($files as $file) {
        $this->createFilesAndAddData($file);
      }
      $this->compileFiles();
      //new SavingCssToDisk($this->compiledScss, $css_file_value_key);
    }

    return [
      "compiled_response" => $this->compiledScss,
    ];
  }

}
