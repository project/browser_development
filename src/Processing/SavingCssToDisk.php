<?php

namespace Drupal\browser_development\Processing;

/**
 * Once SCSS has been compiled SavingCssToDisk writes compressed CSS to disk.
 *
 * @package Drupal\browser_development\Processing
 */
class SavingCssToDisk extends FileSystemStructure {

  /**
   * Default constructor.
   *
   * @param mixed $data
   *   Customers CSS styles.
   */
  public function __construct($data = "{default}") {

    parent::__construct();
    $this->writeToDisk($data);
  }

  /**
   * Writing CSS to file.
   *
   * @param string $data
   *   Data to be added to the CSS file.
   */
  private function writeToDisk($data): void {

    $this->writeToFile(
      print_r($data, 1)
    );
    \Drupal::service('cache.render')->invalidateAll();
  }

}
