<?php

namespace Drupal\browser_development\Processing;

/**
 * Browser Development FormsStorage class.
 *
 * @package Drupal\browser_development\Processing
 */
class FormsStorage {

  /**
   * Create storage.
   *
   * @param string $formName
   *   Form name creator.
   * @param string $input
   *   String creator.
   */
  protected static function storage($formName,$input) {

    $config = \Drupal::service('config.factory')
      ->getEditable('browser_development.settings');
    $config
      ->set('form_arrays', [$formName => $input])
      ->save();
  }

  /**
   * Get storage.
   *
   * @return array
   *   Returns storage contained in database.
   */
  public static function getStorage() {

    return \Drupal::config('browser_development.settings')->get('form_arrays');
  }

  /**
   * Sets storage.
   *
   * @param string $formName
   *   Form name creator.
   * @param string $input
   *   Adds String to storage.
   */
  public static function setStorage($formName, $input) {

    self::storage($formName, $input);

  }

}
