<?php

namespace Drupal\browser_development\Processing;

/**
 * Browser Development Storage class.
 *
 * @package Drupal\browser_development\Processing
 */
class Storage {

  /**
   * Create storage.
   *
   * @param array $inputArray
   *   Array creator.
   */
  protected static function storage(array $inputArray) {

    /**
     * Creates a random number for the machine name.
     */
    function generateRandomString($length = 10) {
      return substr(str_shuffle(
               str_repeat($x = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                  ceil($length / strlen($x)))), 1, $length);
    }

    // Gets date so that it can ge used in display and the machine name.
    $date = date("Y_m_d__H_i_s");

    // Id get a unique random string for the machine name.
    // Label save date it was created.
    // Json_obj adds serialized SCSS object.
    \Drupal::entityTypeManager()
      ->getStorage('browser_development_storage')
      ->create(
        [
          "id" => $date . "__" . generateRandomString(5),
          "label" => $date,
          "json_obj" => serialize($inputArray),
        ]
     )->save();

  }

  /**
   * Get storage.
   *
   * @return array
   *   Returns storage contained in database.
   */
  public static function getStorage() {

    // Loads all options.
    $entity = \Drupal::entityTypeManager()
      ->getStorage('browser_development_storage')
      ->loadMultiple();

    // If entity does on exist then return null.
    if (empty(array_key_last($entity))) {
      return unserialize('{}');
    }

    // Returns the last one to be created.
    $entityLoad = \Drupal::entityTypeManager()
      ->getStorage('browser_development_storage')
      ->load(array_key_last($entity));

    return unserialize($entityLoad->jsonObj());

  }

  /**
   * Sets storage.
   *
   * @todo Follow up the issues below.
   *       | ERROR | PHP4 style constructors are not allowed;
   *       use| | "__construct()" instead 44 | ERROR |
   *       Type hint "json" missing for $inputArray
   *
   * @param json $inputArray
   *   Adds array to storage.
   */
  public static function setStorage($inputArray) {

    self::storage($inputArray);
  }

  /**
   * Displays all versions in storage.
   *
   * @return array
   *   Returns storage contained in database.
   */
  public static function returnAllVersions() {

    return \Drupal::entityTypeManager()
      ->getStorage('browser_development_storage')
      ->loadMultiple();
  }

}
