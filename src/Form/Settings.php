<?php

namespace Drupal\browser_development\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\browser_development\Processing\FormsStorage;

/**
 * Settings page for editor variables.
 *
 * Drupal\browser_development\Form.
 */
class Settings extends FormBase {

  /**
   * Forms unique identifier.
   *
   * @return string
   *   Returns form unique identifier string
   */
  public function getFormId() {
    return 'browser_development_settings';
  }

  /**
   * Build form array.
   *
   * @param array $form
   *   Adds form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Gets form state including input values.
   *
   * @return array
   *   Returns form array elements .
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $base_path = "public://browser-development/";
    $uri = "";
    $website = "";
    $key = "";
    $js = "";

    if (!empty(FormsStorage::getStorage('browser_development_settings')['browser_development_settings'])) {
      if (!empty(FormsStorage::getStorage('browser_development_settings')['browser_development_settings']['base_path'])) {
        $base_path = FormsStorage::getStorage('browser_development_settings')['browser_development_settings']['base_path'];
      }
      $uri = FormsStorage::getStorage('browser_development_settings')['browser_development_settings']['uri'];
      $website = FormsStorage::getStorage('browser_development_settings')['browser_development_settings']['website'];
      $key = FormsStorage::getStorage('browser_development_settings')['browser_development_settings']['key'];
      $js = FormsStorage::getStorage('browser_development_settings')['browser_development_settings']['js'];
    }
    else {
      $base_path = $form_state->getValue('base_path');
      $uri = $form_state->getValue('api_uri');
      $website = $form_state->getValue('api_website');
      $key = $form_state->getValue('api_key');
      $js = $form_state->getValue('js');
    }

    $form['base_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base Path'),
    // Set until test on other classes are created and inline services are removed.
      '#default_value' => 'public://browser-development/',
      '#attributes' => ['readonly' => 'readonly'],
    ];

    $form['api_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Api Url'),
      '#default_value' => $uri,
    ];

    $form['api_website'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Website'),
      '#default_value' => $website,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Api Key'),
      '#default_value' => $key,
    ];

    $form['js'] = [
      '#type' => 'textfield',
      '#title' => $this->t('JS library'),
      '#default_value' => $js,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Validation method.
   *
   * @param array $form
   *   Array form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Gets form state including input values.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Form submit method.
   *
   * @param array $form
   *   Array form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Gets form state including input values.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    FormsStorage::setStorage('browser_development_settings', [
      'base_path' => $form_state->getValue('base_path'),
      'uri' => $form_state->getValue('api_uri'),
      'website' => $form_state->getValue('api_website'),
      'key' => $form_state->getValue('api_key'),
      'js' => $form_state->getValue('js'),
    ]);

    $uriStorage = FormsStorage::getStorage('browser_development_settings')['browser_development_settings'];
    $this->messenger()->addMessage($this->t('API information @api.', ['@api' => print_r($uriStorage, TRUE)]));
  }

}
