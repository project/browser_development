<?php

namespace Drupal\browser_development\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class BrowserDevelopmentStorageForm.
 */
class BrowserDevelopmentStorageForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $browser_development_storage = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $browser_development_storage->label(),
      '#description' => $this->t("Label for the Browser development storage."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $browser_development_storage->id(),
      '#machine_name' => [
        'exists' => '\Drupal\browser_development\Entity\BrowserDevelopmentStorage::load',
      ],
      '#disabled' => !$browser_development_storage->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $browser_development_storage = $this->entity;
    $status = $browser_development_storage->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Browser development storage.', [
          '%label' => $browser_development_storage->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Browser development storage.', [
          '%label' => $browser_development_storage->label(),
        ]));
    }
    $form_state->setRedirectUrl($browser_development_storage->toUrl('collection'));
  }

}
